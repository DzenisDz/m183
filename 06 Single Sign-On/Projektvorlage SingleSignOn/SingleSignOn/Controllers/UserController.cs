using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Google.Apis.Auth;
using Google.Apis.Auth.OAuth2.Requests;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SingleSignOn.Models;
using SingleSignOn.Models.DataTransferObjects;
using SingleSignOn.Services;

namespace SingleSignOn.Controllers
{
    public class UserController : Controller
    {
        /**
         * The user manager is a handy tool for various operations regarding User objects.
         * It might be used to create and/or retrieve users.
         */
        private readonly UserManager<User> _userManager;

        /**
         * Constructor of the UserController.
         * The userManager dependency is provided by dependency injection.
         */
        public UserController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        /**
         * This action is used for verification of the provided id token.
         * If the token is valid, an existing user with the validated user data is retrieved from the user store (using the _userManager).
         * If no user with the validated user data exists, a new user object is created and added to the user user store.
         *
         * Finally, the (existing|created) user gets authenticated and a http status 200 (Ok) is returned.
         *
         * If the validation of the id token with the identity providers backend fails, a http status 400 (BadRequest) is returned.
         */
        [HttpPost]
        public async Task<IActionResult> VerifyIdToken([FromBody] IdTokenVerificationRequest verificationRequest)
        {
            // TODO: Implement this method according to the above comment.
            bool valid = true;
            GoogleJsonWebSignature.Payload payload = await GoogleJsonWebSignature.ValidateAsync(verificationRequest.IdToken);
            if (!payload.Audience.Equals("676941325549-sfeap8l76ga67i7544g6i5ljsmm0bj04.apps.googleusercontent.com"))
                valid = false;
            if (!payload.Issuer.Equals("accounts.google.com") && !payload.Issuer.Equals("https://accounts.google.com"))
                valid = false;
            if (payload.ExpirationTimeSeconds == null)
                valid = false;
            else
            {
                DateTime now = DateTime.Now.ToUniversalTime();
                DateTime expiration = DateTimeOffset.FromUnixTimeSeconds((long)payload.ExpirationTimeSeconds).DateTime;
                if (now > expiration)
                {
                    valid = false;
                }
            }

            if(false == valid)
            {
                return BadRequest();
            }

            User user = await _userManager.FindByEmailAsync(payload.Email);

            if (null == user)
            {
               user = await CreateUser(payload.Email, payload.Name, payload.FamilyName);
            }

            await AuthenticateUser(user);

            return Ok("OK");
        }

        /**
         * Creates a new user object with the given data and adds it to the user store.
         * If the creation and persistence of the user object succeeded, the user object is returned. In case of failure, null is returned.
         */
        private async Task<User> CreateUser(string email, string firstName, string lastName)
        {
            var user = new User
            {
                UserName = email,
                Email = email,
                FirstName = firstName,
                LastName = lastName,
            };
            var result = await _userManager.CreateAsync(user);

            return result.Succeeded ? user : null;
        }

        /**
         * Authenticates the given user object using the cooke authentication scheme.
         * The cookie (and therefore the session) will last for 1 day.
         */
        private async Task AuthenticateUser(User user)
        {
            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                IsPersistent = true,
                ExpiresUtc = DateTimeOffset.Now.AddDays(1),
            };

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(UserService.CreateClaimsIdentity(user)),
                authProperties
            );
        }
    }
}