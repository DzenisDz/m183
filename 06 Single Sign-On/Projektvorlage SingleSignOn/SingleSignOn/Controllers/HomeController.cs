﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using SingleSignOn.Models;

namespace SingleSignOn.Controllers
{
    public class HomeController : Controller
    {
        /**
         * WELCOME page
         * Upon application start, this action will be called.
         */
        public IActionResult Index()
        {
            return View();
        }

        /**
         * Displays the login page.
         * Add the single sign-on authentication buttons in the view related to this action.
         */
        public IActionResult Login()
        {

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}