using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SingleSignOn.Models;

namespace SingleSignOn.Controllers
{
    public class ProfileController : Controller
    {
        /**
         * The user manager is a handy tool for various operations regarding User objects.
         * It might be used to create and/or retrieve users.
         */
        private readonly UserManager<User> _userManager;

        /**
         * Constructor of the ProfileController.
         * The userManager dependency is provided by dependency injection.
         */
        public ProfileController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        /**
         * Displays the users profile data.
         * The Authorize attribute protects this action: Only authenticated user may call it.
         */
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            ViewBag.User = user;
            return View();
        }

        /**
         * Logout handler - ends the currently authenticated users session.
         */
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return View();
        }
    }
}