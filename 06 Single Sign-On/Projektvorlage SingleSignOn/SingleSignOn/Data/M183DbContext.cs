using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SingleSignOn.Models;

namespace SingleSignOn.Data
{
    public class M183DbContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public M183DbContext(DbContextOptions<M183DbContext> options) : base(options)
        {
        }
    }
}