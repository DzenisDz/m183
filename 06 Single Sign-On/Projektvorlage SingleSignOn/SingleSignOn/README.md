# Single Sign-On
Diese Projektvorlage kann für die Bearbeitung der Praxisaufgabe zum Thema _Single Sign-On_ im Modul 183 am GIBZ genutzt werden.

## Vorgehen
Studieren Sie zuerst die nachfolgenden Beschreibungen des Login- und Logout-Prozess. Für die erfolgreiche Umsetzung ist es wichtig, dass Sie das zweistufige Verfahren der Authentifizierung (_Identity Provider_ und lokal) verstehen.

Gehen Sie anschliessend am besten _schrittweise_ vor, indem Sie zuerst den Login-Prozess und anschliessend den Logout-Prozess implementieren. Bei der Umsetzung sollen Sie sich primär auf die Implementation des Single Sign-On Verfahrens kümmern. Es sind daher zahlreiche Methoden und Funktionen bereits vollständig oder teilweise implementiert. Innerhalb des Quellcodes können Sie sich an den **TODO** Kommentaren orientieren. Oft finden Sie dort auch Links zu weiterführenden Informationen. 

## Login-Prozess
Der Login-Prozess für diese Applikation ist zweistufig. Im ersten Schritt meldet sich die Nutzerin bzw. der Nutzer mit den Login-Daten von Google beim _Identity Provider_ an. Sofern diese Authentifizierung erfolgreich war, wird die Bestätigung des _Identity Providers_, zusammen mit den persönlichen Daten, für die lokale Authentifizierung in dieser Applikation verwendet.

Der Prozess verläuft folgendermassen:

1. Die Benutzerin bzw. der Benutzer klickt im Frontend auf der Login-Seite auf den Google-Button
2. Es öffnet sich ein neues Fenster, in welchem sich die Benutzerin bzw. der Benutzer mit den persönlichen Google-Credentials anmelden kann.
3. Bei einem erfolgreichen Login-Versuch mit Google wird die JavaScript-Funktion `onSuccess` in der Datei `wwwroot/js/googleSignIn.js` aufgerufen.
4. Die `onSuccess`-Funktion sendet den ID Token von Google ans Backend dieser Applikation. Dazu kann beispielsweise die JavaScript-Funktion `fetch` verwendet werden.
5. In der Action `VerifyIdToken` des `UserController` wird dieser ID Token validiert. Dazu kann die statische Methode `GoogleJsonWebSignature.ValidateAsync` aus der NuGet-Packet `Google.Apis.Auth` verwendet werden.
6. Wenn die Validierung fehlschlägt, wird der gesamte Login-Prozess abgebrochen. Es kann das Ergebnis der Methode `BadRequest()` als Rückgabewert verwendet werden.
7. Wenn die Validierung erfolgreich ist, wird geprüft, ob bereits ein Benutzer mit den entsprechenden persönlichen Daten innerhalb dieser Applikation existiert. Dazu können die Informationen verwendet werden, welche in der zuvor durchgeführten Validierung als Rückgabewert resultieren.
8. Wenn noch kein entsprechendes `User`-Objekt existiert, wird dieses erstellt und persistiert. Dazu kann die private Methode `CreateUser` im `UserController` verwendet werden.
9. Wenn bereits ein entsprechendes `User`-Objekt existiert, wird dieses (unter Verwendung einer Instanz des `UserManager<User>`) geladen.
10. Im Backend dieser Applikation wird nun für das neu erstellte bzw. bereits existierende Objekt die lokale Authentifizierung durchgeführt. Dazu kann die private Methode `AuthenticateUser` im `UserController` verwendet werden.
11. Durch die erfolgreiche Authentifizierung - angezeigt mit dem Ergebnis der Methode `Ok()` als Rückgabewert - wird der Authentifizierungs-Prozess im Frontend fortgesetzt.
12. Wenn die Validierung in der `onSuccess` Methode erfolgreich verläuft, ist die entsprechende Benutzerin bzw. der entsprechende Benutzer nun erfolgreich authentifiziert.
13. Es kann nun mittels JavaScript eine Weiterleitung auf die Profil-Seite durchgeführt werden. Dazu kann beispielsweise die Funktion `window.location.replace` verwendet werden.

## Logout-Prozess
Der Logout-Prozess verläuft analog zum Login-Prozess zweistufig, jedoch in umgekehrter Reihenfolge: Zuerst wird die Nutzerin bzw. der Nutzer lokal in der Applikation abgemeldet. Anschliessend findet auch eine Abmeldung beim _Identity Provider_ (Google) statt.

1. Die Benutzerin bzw. der Benutzer klickt auf der Profil-Seite auf den Logout-Button.
2. Dadurch wird die `Logout` Action im `ProfileController` aufgerufen.
3. Mit der Ausführung der `Logout` Action des `ProfileController` wird die Nutzerin bzw. der Nutzer lokal abgemeldet. Nach Abschluss dieses Prozess wird die entsprechende `Logout` View angezeigt.
4. Mit dem Laden und Anzeigen der `Logout` View wird die JavaScript-Funktion `signOut` der Datei `wwwroot/js/googleSignOut.js` ausgeführt.
5. Die Hauptaufgabe dieser `signOut` Funktion liegt in der Beendigung der Session mit dem _Identity Provider_ (Google). Dazu kann die entsprechende Funktion der Google-Bibliothek verwendet werden.