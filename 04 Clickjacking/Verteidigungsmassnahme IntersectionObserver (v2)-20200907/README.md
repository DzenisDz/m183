# M183: Demo-Projekt für Clickjacking
In diesem Projekt wird exemplarisch die Funktionsweise einer Clickjacking-Attacke aufgezeigt.

## Ausgangslage

Ein (fiktiver) Angreifer versucht ahnungslose Besucher/-innen seiner manipulierten Webseite zu überlisten. Dazu wird eine "externe" Webseite (`innerPage/inner.html`) in einem `iframe` auf der Webseite des Angreifers (`outer.html`) eingebunden. Das ganze `iframe` wird anschliessend durch fingierte Inhalte des Angreifers überlagert.

Die Aufgabe der Lernenden besteht darin, als Betreiber/-in der _externen_ Webseite die hier gezeigte Clickjacking-Attacke zu unterbinden. Dazu soll eine relativ neue Strategie mit [IntersectionObserver](https://w3c.github.io/IntersectionObserver/v2/) (Version 2) angewendet werden.

Eine entsprechende Arbeitsanweisung mit verschiedenen Links ist in der Datei [innerPage/inner.html](innerPage/inner.html) (ab Zeile 110) zu finden.

## Arbeitsauftrag

Sie kennen zwei verschiedene Möglichkeiten, wie Clickjacking SERVERSEITIG verhindert werden kann. Für diese Aufgabe haben Sie jedoch keinen Zugriff auf den Server. Sie müssen daher Clickjacking-Angriffe **clientseitig** verhindern. Verwenden Sie dafür einen *IntersectionObserver* (v2). Dies erfordert den Einsatz des Browsers *Chrome* (ab Version 74), *Edge* (ab Version 79) oder *Opera* (ab Version 62).

Sie können sich bei der Umsetzung auf folgende Ressourcen abstützen:

- https://www.afasterweb.com/2019/02/28/proposed-updates-for-intersection-observer/
- https://developers.google.com/web/updates/2019/02/intersectionobserver-v2
- https://www.youtube.com/watch?v=EIH6IQgwdAc
- https://io-v2.glitch.me/ (Beispiel aus dem oben verlinkten Video)

Die Umsetzung gilt als erfolgreich, wenn kein "Betrug" (bzw. Irreführung") mehr stattfinden kann. Verwenden Sie dazu die Klasse, welche in der Variable `indicatorClass`  vorbereitet ist zur Kennzeichnung des Buttons: Fügen Sie die Klasse zum Button hinzu, wenn dieser (teilweise) verdeckt ist. Andernfalls soll die Klasse entfernt werden.

**Wichtig:** Sie wollen verhindern, dass unbeabsichtigte Klicks auf Ihrer Webseite erfolgen. Sie können daher nur den Code in der Datei `innen.html` editieren. Der Code in der Datei `outer.html` wird von der Angreiferin bzw. dem Angreifer geschrieben und kann durch Sie nicht kontrolliert werden.