# M183 Dzenis Dzananovic
In diesem Modul haben wir jede Woche eine Aufgabe gemacht. Diese Aufgaben geben Punkte für die pünktliche abgabe, das Erreichen der Minimalanforderungen und für Erweiterungen, die man entweder selber entscheidet oder man kann eine vorgeschlage Erweiterung machen.

## 02 XSS Keylogger
Bei dieser Aufgabe mussten wir bei einer Webseite die Tasteneingaben eines Users, entweder gruppiert als Wörter oder Sätze, an einem Endpunkt, der schon bereit gemacht wurde, schicken. Dies sollten wir mit JavaScript machen

Ich habe bei meiner Lösung die Eingaben als Wörter gruppiert und geschickt und für die Erweiterung habe ich noch die Mausklicks auch an den Endpunkt geschickt.

## 03 Zweifaktor-Authentifizierung mit TOTP
Bei dieser Aufgabe mussten wir eine Webseite machen, bei der man sich anmelden kann und nachdem man sich anmeldet, wird noch eine TOTP (Time-based One-Time-Password) gefordert. Es wurde uns eine Vorlage vorbereitet, die wir benutzen dürfen. 

Ich wollte bei dieser Aufgabe noch eigene Einstellungen machen, z.B. die Zeit die es braucht um das Passwort zu ändern, aber ich konnte es nicht machen und dann habe ich einfach nur die Mindestanforderungen gemacht.

## 04 UI Redressing (Clickjacking)
Bei dieser Aufgabe mussten wir eine Webseite erstellen, die eine andere Webseite einbinden und dann müssen wir UI Elemente hinzufügen, die sich mit den Elementen der eingebundene Webseite überlappen, um die User zu täuschen und ihre eingaben zu bekommen.

Ich habe eine Webseite der Schule benutzt, wo man sich einfach mit seinem Account der Schule anmeldet. Nachdem man sich anmeldet schickt es die Daten mit einer HTTP-Request an einem schon vorbereiteten Endpunkt. Danach leitet es den User zur richtigen Webseite, damit er denkt es hätte einfach nicht funktioniert.

## 05 Sichere Passwörter
Bei dieser Aufgabe mussten wir einfach recherchieren, was ein sicheres Passwort ausmacht und dann mussten wir noch eine Webseite erstellen bei der man ein Account erstellt und beim Passwort muss man bestimmte Zeichen drinnen haben. Diese Aufgabe konnten wir zu zweit machen.

## 06 Single Sign-On
Bei dieser Aufgabe mussten wir eine Webseite machen, bei der man sich mit seinem Googleaccount anmelden kann. Danach soll es einfach den Namen und die Email anzeigen. Es wurde wieder eine Vorlage vorbereitet, die wir benutzen dürfen.

Ich habe die Vorlage benutzt und nur die Mindestanforderungen gemacht.

## 07 HTTP Authentication
Bei dieser Aufgabe mussten wir bei der Anmeldung, die Daten mit HTTP Digest an einem schon vorbereitet Endpunkt schicken. Es wurde eine Vorlage vorbereitet, die man benutzen darf, um diese Aufgabe zu lösen. Die Zugangsdaten wurden auch schon vorbereitet.

Ich habe die Vorlage benutzt und habe alle Mindestanforderungen erreicht. Die Vertiefung habe ich auch noch gemacht. Man musste man noch recherchieren wie man HTTP Digest serverseitig implementiert. Danach musste man einfach noch Beispiel Dateien im Route hinzufügen.

## 08 Cross-Site Request Forgery
Bei dieser Aufgabe mussten wir eine Webseite machen, bei der es Eingabefelder hat mit denen man, auf einem Account auf einer vorbereiteten Webseite, das Passwort ändern, die Email ändern und einen Blog-Post erstellen kann.

Das Passwort habe ich mit einem normalen Formular gelöst und für die Email und den Blog-Post habe ich JavaScript benutzt.