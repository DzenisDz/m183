﻿using System;
using System.Threading.Tasks;
using Google.Authenticator;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Praxisarbeit_2FA.Data;

namespace Praxisarbeit_2FA.Controllers
{
    public class HomeController : Controller
    {
        private readonly M183DbContext _context;
        private readonly ILogger<HomeController> _logger;
        private string _uniqueKey = "DzenisKeyDZ";
        private bool test = false;

        public HomeController(M183DbContext context, ILogger<HomeController> logger)
        {
            _context = context;
            _logger = logger;

            context.Database.EnsureCreated();
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromForm] string username, [FromForm] string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == username);
            if (user == null)
            {
                // User not found - credentials are invalid
                ViewBag.Message = "Wrong Credentials";
                return View();
            }

            if (user.Password == password)
            {
                // First step of authentication completed (correct passwort).

                if (user.TwoFactorAuthIsRegistered == true)
                {
                    ViewBag.isAlreadyRegistered = true;
                }
                
                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                var setupInfo = tfa.GenerateSetupCode("m183", user.Username, _uniqueKey, false, 300);

                ViewBag.QRUrl = setupInfo.QrCodeSetupImageUrl;
                ViewBag.ManualEntryKey = setupInfo.ManualEntryKey;
                ViewBag.Username = username;

                // TODO: Implement 2FA
                // Either setup totp or present form to enter totp
                return View("TwoFactorAuth");
            }

            // User found but incorrect passwort
            ViewBag.Message = "Wrong Credentials";
            ViewBag.AlertType = "danger";
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> TwoFactorAuth([FromForm] string key, [FromForm] string qrCodeUrl, [FromForm] string manualEntryKey, [FromForm] string username)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == username);

            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            bool isCorrectPIN = tfa.ValidateTwoFactorPIN(_uniqueKey, key);

            if (isCorrectPIN)
            {
                if (user.TwoFactorAuthIsRegistered == false)
                {
                    user.TwoFactorAuthIsRegistered = true;
                    _context.Entry(user).State = EntityState.Modified;
                    _context.SaveChanges();
                }
               
                return View("UserProfile");
            }

            if (user.TwoFactorAuthIsRegistered == true)
            {
                ViewBag.isAlreadyRegistered = true;
            }

            ViewBag.QRUrl = qrCodeUrl;
            ViewBag.ManualEntryKey = manualEntryKey;
            ViewBag.Username = username;

            // User found but incorrect passwort
            ViewBag.Message = "Wrong Code Try Again";
            ViewBag.AlertType = "danger";
            return View();
        }

        public async Task<IActionResult> UserProfile(Guid userId)
        {
            var user = await _context.Users.FindAsync(userId);
            ViewBag.Username = user.Username;
            return View();
        }

    }
}